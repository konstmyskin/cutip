//
//  ViewController.swift
//  Cutip
//
//  Created by Konstantin Myskin on 13/01/2018.
//  Copyright © 2018 Konstantin Myskin. All rights reserved.
//

import UIKit
import EFQRCode

class CardsViewController: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var menu: UIButton!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var oneCardControls: UIStackView!
    @IBOutlet weak var exitButton: UIButton!
    @IBOutlet weak var exitButtonWidth: NSLayoutConstraint!
    @IBOutlet weak var toExitButtonWidth: NSLayoutConstraint!
    @IBOutlet weak var whole: UILabel!
    @IBOutlet weak var part: UILabel!
    @IBOutlet weak var billboard: UIView!
    //    @IBOutlet weak var floaty: Floaty!
    var stacksVC: StacksViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.tweakUI), name: NSNotification.Name(rawValue: NotificationName.updateCoin), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.attributedString), name: NSNotification.Name(rawValue: NotificationName.slideCoin), object: nil)
        whole.layer.cornerRadius = 5
        tweakUI()
    }
    
    @objc func tweakUI() {
        if AppManager.℠.currentMode != .oneCard {
            exitButton.alpha = 0
            self.name.text = AppManager.℠.currentCoin?.name
            let font = UIFont.init(name: "Futura-MediumItalic", size: 23)
            let attrs = [
                NSAttributedStringKey.foregroundColor: UIColor.white,
                NSAttributedStringKey.font: font,
                NSAttributedStringKey.kern: 1.2
                ] as [NSAttributedStringKey : Any]
            whole.attributedText = NSMutableAttributedString(string: AppManager.℠.code(for: AppManager.℠.currentCoin!), attributes: attrs) 
        } else {
            exitButton.alpha = 1
        }
    }
    
    @IBAction func eject(_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.eject), object: nil)
    }
    
    @objc func attributedString(noti:NSNotification) {
        let index = noti.object as! Int
        let nonBoldRange = kRanges[index]
        let font = UIFont.init(name: "Futura-MediumItalic", size: 23)
        let attrs = [
            NSAttributedStringKey.underlineStyle: 0,
            NSAttributedStringKey.font: font,
            NSAttributedStringKey.kern: 2
            ] as [NSAttributedStringKey : Any]
        let font2 = UIFont.init(name: "Futura-MediumItalic", size: 30)
        let nonBoldAttribute = [
            NSAttributedStringKey.foregroundColor: UIColor.yellow,
            NSAttributedStringKey.underlineStyle: 1,
            NSAttributedStringKey.font: font2,
            NSAttributedStringKey.kern: 2.5
            ] as [NSAttributedStringKey : Any]
        let attrStr = NSMutableAttributedString(string: AppManager.℠.code(for: AppManager.℠.currentCoin!), attributes: attrs)
//        if index > 0 {
//            attrStr.insert(NSAttributedString.init(string:"+"), at: nonBoldRange.location + index)
//        }
        attrStr.setAttributes(nonBoldAttribute, range: nonBoldRange)
        self.whole.attributedText = attrStr
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "containerSegue" {
            self.stacksVC = segue.destination as? StacksViewController
        }
    }
    
}
