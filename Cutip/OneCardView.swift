//
//  OneCardView.swift
//  Cutip
//
//  Created by Konstantin Myskin on 13/01/2018.
//  Copyright © 2018 Konstantin Myskin. All rights reserved.
//

import UIKit
import EFQRCode 

class OneCardView: UIView {
    
    @IBOutlet weak var imageActually: UIImageView!
    @IBOutlet weak var back: UIView!
    @IBOutlet weak var title: UILabel!
    var index: Int!
    
    override func awakeFromNib() {
        super.awakeFromNib() 
        generateImage()
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateCoin(noti:)), name: NSNotification.Name(rawValue: NotificationName.updateCoin), object: nil)
    }
    
    @objc func updateCoin(noti:NSNotification) {
        let indx = noti.object as! Int
        if indx == self.index {
            let coin = AppManager.℠.coins[index]
            if AppManager.℠.currentMode == .oneCard {
                back.alpha = 1
                
                switch tag {
                case 0:
                    title.text = "Ticker\nName"
                case 1:
                    title.text = "Instance index\n+\nChecksum"
                case 2:
                    if coin.contract != "" {
                        let font = UIFont.init(name: "Futura-Medium", size: 33)
                        let font2 = UIFont.init(name: "Futura-MediumItalic", size: 23) 
                        let attrs = [
                            NSAttributedStringKey.foregroundColor: UIColor.darkText,
                            NSAttributedStringKey.font: font,
                            NSAttributedStringKey.kern: 1.2
                            ] as [NSAttributedStringKey : Any]
                        var str1 = NSMutableAttributedString()
                        if let type = kCoinTypes[coin.type] {
                            str1 = NSMutableAttributedString(string: "Blockchain type\n(\(kCoinTypes[coin.type]))\n", attributes: attrs)
                        } else {
                            str1 = NSMutableAttributedString(string: "Blockchain type\n(PoA, Utility)\n", attributes: attrs)
                        }
                        let attrs2 = [
                            NSAttributedStringKey.foregroundColor: UIColor.darkText,
                            NSAttributedStringKey.font: font2,
                            NSAttributedStringKey.kern: 1
                            ] as [NSAttributedStringKey : Any]
                        str1.append(NSMutableAttributedString(string: "(Contract address: \(coin.contract)\n", attributes: attrs2))
                        
                        if coin.name != "POA" {
                            str1.append(NSMutableAttributedString(string: "+\nCountry", attributes: attrs))
                        } else {
                            str1.append(NSMutableAttributedString(string: "+\nCayman Islands", attributes: attrs))
                        }
                        title.attributedText = str1
                    } else {
                        title.text = "Blockchain type\n(\(kCoinTypes[coin.type]!))\n+\nCountry"
                    }
                case 3:
                    title.text = "Entity type\n+\nSecurity/Utility (Optional)"
                case 4:
                    title.text = "Initial date"
                case 5:
                    title.text = "Hash type\n(\(kHashTypes[coin.hash]!))\n+\nReserved Fields (Cusip, etc.)"
                default:
                    break
                }
            } else {
                generateImage()
                back.alpha = 0
            }
        }
    }
    
    func generateImage() {
        let str = AppManager.℠.code(for: AppManager.℠.currentCoin!) + " " + AppManager.℠.currentCoin!.contract
        let generator = EFQRCodeGenerator(content: str, size: EFIntSize.init(width: 200, height: 200))
        //        generator.setMode(mode: EFQRCodeMode)
        //        generator.setInputCorrectionLevel(inputCorrectionLevel: EFInputCorrectionLevel)
        //        generator.setSize(size: EFIntSize)
        //        generator.setMagnification(magnification: EFIntSize?)
        //        generator.setColors(backgroundColor: CIColor, foregroundColor: CIColor)
        //        generator.setIcon(icon: UIImage(named: "logo")?.toCGImage(), size: EFIntSize.init(width: 200, height: 200))
        generator.setWatermark(watermark: UIImage(named: AppManager.℠.currentCoin!.name ?? "Bitcoin")?.toCGImage(), mode: EFWatermarkMode.scaleAspectFit)
        //        generator.setForegroundPointOffset(foregroundPointOffset: CGFloat)
        generator.setAllowTransparent(allowTransparent: false)
        generator.setPointShape(pointShape: .circle)
                generator.setBinarizationThreshold(binarizationThreshold: 0.8) 
        
        // Final two-dimensional code image we get
        
        imageActually.image = UIImage.init(cgImage: generator.generate()!)
    }
    
    func recognizeImage() {
        if let testImage = UIImage(named: AppManager.℠.currentCoin!.name ?? "Bitcoin")?.toCGImage() {
            if let tryCodes = EFQRCode.recognize(image: testImage) {
                if tryCodes.count > 0 {
                    print("There are \(tryCodes.count) codes in testImage.")
                    for (index, code) in tryCodes.enumerated() {
                        print("The content of \(index) QR Code is: \(code).")
                    }
                } else {
                    print("There is no QR Codes in testImage.")
                }
            } else {
                print("Recognize failed, check your input image!")
            }
        }
    }
}

//lady alien pottery short write shock ticket shield together tired chunk brick
