//
//  StacksViewController.swift
//  Cutip
//
//  Created by Konstantin Myskin on 13/01/2018.
//  Copyright © 2018 Konstantin Myskin. All rights reserved.
//

import UIKit

struct Stack {
    var count: Int
    var index: Int
    var name: String
    var aspects: Array<Aspect>  
}

struct Aspect {
    var index: Int
    var text: String
}

class StacksViewController: UIPageViewController {
    
    var currentIndex = 0
    var pretendentIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        tweakUI(noti: nil) 

        // Do any additional setup after loading the view.
    }
    
    func tweakUI(noti:NSNotification?) {
        switch AppManager.℠.currentMode {
        case .browse:
            self.dataSource = self
            self.setViewControllers(self.createSingleStack(withIndex: self.currentIndex, count: 1), direction: UIPageViewControllerNavigationDirection.forward, animated: false) { (Bool) in }
        case .oneCard:
            self.dataSource = nil
            self.setViewControllers(self.createSingleStack(withIndex: self.currentIndex, count: 5), direction: UIPageViewControllerNavigationDirection.forward, animated: false) { (Bool) in }
        }
    }

    func createSingleStack(withIndex index:Int, count:Int) -> Array<SingleStackViewController> {
        var bunch: Array<SingleStackViewController> = Array()
        let single = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier:"SingleStackViewController") as! SingleStackViewController
        var stack = Stack.init(count: count, index: index, name:"Name", aspects: Array())
        for _ in 0..<count {
            stack.aspects.append(Aspect(index:0, text:""))
        }
        stack.index = index
        single.stack = stack
        bunch.append(single)
        return bunch
    }

}

extension StacksViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        let one = pendingViewControllers[0] as! SingleStackViewController
        self.pretendentIndex = one.stack.index 
        AppManager.℠.currentCoin = AppManager.℠.coins[self.pretendentIndex]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.updateCoin), object: one.stack.index)
    }
}

extension StacksViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            self.currentIndex = self.pretendentIndex
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let nextIndex = self.currentIndex - 1
        if nextIndex > -1 {
            return self.createSingleStack(withIndex: nextIndex, count: 1)[0]
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let nextIndex = self.currentIndex + 1
        if nextIndex < AppManager.℠.coins.count {
            return self.createSingleStack(withIndex: nextIndex, count: 1)[0]
        }
        return nil
    }
}
