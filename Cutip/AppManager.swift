//
//  AppManager.swift
//  Cutip
//
//  Created by Konstantin Myskin on 13/01/2018.
//  Copyright © 2018 Konstantin Myskin. All rights reserved.
//

import Foundation

struct NotificationName {
    static let reloadMode = "reloadMode"
    static let updateCoin = "updateCoin"
    static let slideCoin = "slideCoin"
    static let eject = "eject" 
}
 
enum cardsMode {
    case browse
    case oneCard
}

let kRanges = [NSMakeRange(0, 6), NSMakeRange(6, 3), NSMakeRange(8, 3), NSMakeRange(11, 2), NSMakeRange(13, 4), NSMakeRange(17, 2)]



final class AppManager {
    
    static let ℠ = AppManager()
    var currentMode: cardsMode = .browse
    var coins:[Coin] = []
    var currentCoin:Coin? = nil
    
    private init() {
        fillCoins()
    }
    
    func fillCoins() {
        coins = [
            Coin.init(name: "Bitcoin", symbol: "BTC", type: "1", country: "1", entity: "1", date: "9109", hash: "1", contract:""),
        Coin.init(name: "Bitcoin Cash", symbol: "BCH", type: "1", country: "1", entity: "1", date: "8117", hash: "B", contract:""),
        Coin.init(name: "Propy", symbol: "PRO", type: "1", country: "1", entity: "1", date: "8" + kDate[15] + "17", hash: "6", contract:"0x226bb599a12c826476e3a771454697ea52e9e220"),
        Coin.init(name: "Well", symbol: "WELL", type: "1", country: "1", entity: "1", date: "2" + kDate[25] + "18", hash: "6", contract:""),
        Coin.init(name: "POA", symbol: "POA", type: "U", country: "4", entity: "X", date: "2" + kDate[25] + "18", hash: "6", contract:"0x9e8df8fd7c6c724880e2d73d8faf7fc7ff98c5c4"),
        Coin.init(name: "Dash", symbol: "DASH", type: "2", country: "1", entity: "1", date: "1" + kDate[18] + "14", hash: "A", contract:""),
        Coin.init(name: "NEM", symbol: "XEM", type: "4", country: "2", entity: "1", date: "3" + kDate[31] + "15", hash: "3", contract:""),
        Coin.init(name: "SysCoin", symbol: "SYS", type: "1", country: "1", entity: "1", date: "7" + kDate[16] + "14", hash: "4", contract:""),
        Coin.init(name: "Ethereum Classic", symbol: "ETC", type: "1", country: "3", entity: "1", date: "7" + kDate[30] + "15", hash: "6", contract:""),
        Coin.init(name: "Ethereum", symbol: "ETH", type: "1", country: "3", entity: "1", date: "7" + kDate[22] + "14", hash: "6", contract:""),
        Coin.init(name: "Nxt", symbol: "NXT", type: "5", country: "4", entity: "1", date: "9" + kDate[28] + "13", hash: "3", contract:""),
        Coin.init(name: "Siacoin", symbol: "SC", type: "1", country: "1", entity: "1", date: "3" + kDate[16] + "15", hash: "8", contract:""), 
//        Coin.init(name: "Siacoin", symbol: "SC", type: "1", country: "US", entity: "1", date: "3" + kDate[16] + "15", hash: "8")
        ]
        currentCoin = coins[0] 
        
    }
    
    func code(for coin:Coin) -> String {
        var symbol = coin.symbol
        symbol = symbol.padding(toLength: 6, withPad: "0", startingAt: 0)
        let instance = "0"
        let check = "7"
        let security = "0"
        let reserved = "0"
        return symbol + instance + check + "\n" + coin.type + coin.country + coin.entity + security + coin.date + coin.hash + reserved
    }
    

}

let kCoinTypes = ["1" : "PoW", "2" : "PoW/PoS", "3" : "PoS", "4" : "PoI", "5" : "PoS/LPoS", "6" : "ERC20", "7" : "DPoS", "8" : "dBFT", "9" : "PoR", "A" : "PoPP", "B" : "PoA"]
let kCountries = ["1" : "US", "2" : "Ca", "3" : "JP", "4" : "Cayman Islands"]
let kHashTypes = ["1" : "sha256", "2" : "x11", "3" : "sha512", "4" : "scrypt", "5" : "PoS", "6" : "ethash", "7" : "x13", "8" : "Blake2b", "9" : "dpos", "A" : "pos3", "B" : "SigHash"]

struct Coin {
    var name = ""
    var symbol = ""
    var type = ""
    var country = ""
    var entity = ""
    var date = ""
    var hash = ""
    var contract = ""
}
 

let kDate = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X" ]

let coinNames = ["Ark", "Ethereum", "Spectrecoin", "Lisk", "Storj", "Gambit", "Populous", "Augur", "Qtum", "Golem", "OmiseGo", "Edgeless", "Waves", "0x", "iExec RLC", "Santiment", "OpenAnx", "Stox"]

