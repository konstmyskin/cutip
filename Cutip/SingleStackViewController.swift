//
//  SingleStackViewController.swift
//  Cutip
//
//  Created by Konstantin Myskin on 13/01/2018.
//  Copyright © 2018 Konstantin Myskin. All rights reserved.
//

import UIKit
import Koloda

class SingleStackViewController: UIViewController {
    
    @IBOutlet weak var kolodaView: KolodaView!
    var stack: Stack!
    var isMovable: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        kolodaView.dataSource = self
        kolodaView.delegate = self
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(self.koloda(_:didSelectCardAt:)))
        view.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.tweakUI), name: NSNotification.Name(rawValue: NotificationName.reloadMode), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.koloda(_:didSelectCardAt:)), name: NSNotification.Name(rawValue: NotificationName.eject), object: nil)
        
        tweakUI(noti: nil)
    }
    
    @objc func tweakUI(noti:NSNotification?) {
        if AppManager.℠.currentMode == .oneCard {
            self.isMovable = true
        }
        else {
            self.isMovable = false
        }
    }
}

extension SingleStackViewController: KolodaViewDelegate {
    
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        AppManager.℠.currentMode = .browse
        self.reloadStack()
    }
    
    @objc func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
        if AppManager.℠.currentMode == .browse {
            AppManager.℠.currentMode = .oneCard
        } else {
            AppManager.℠.currentMode = .browse
        }
        self.reloadStack()
    }
    
    func reloadStack() {
        DispatchQueue.main.async {
            var stack = Stack.init(count: 1, index: 0, name:"Name", aspects: Array())
            if AppManager.℠.currentMode == .oneCard {
                stack.count = 6
            }
            else {
                stack.count = 1
            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.reloadMode), object: nil)
            for _ in 0..<stack.count {
                stack.aspects.append(Aspect(index: 0, text: ""))
            }
            stack.index = self.stack.index
            self.stack = stack
            self.kolodaView.resetCurrentCardIndex()
            self.tweakUI(noti: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.updateCoin), object: stack.index)
        }
    }
}

extension UIView {
    class func fromNib<T : UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}

extension SingleStackViewController: KolodaViewDataSource {
    func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return DragSpeed.moderate
    }
    
    func kolodaNumberOfCards(_ koloda:KolodaView) -> Int {
        return self.stack!.count
    }
    
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        let base = UIView()
        
        if (self.stack.aspects.count > 0) && index < (self.stack.aspects.count) {
            let view: OneCardView = OneCardView.fromNib()
            view.tag = index
            base.addSubview(view)
            view.translatesAutoresizingMaskIntoConstraints = false
            view.center = base.center
            view.layer.cornerRadius = 10
            base.layer.cornerRadius = 10
            base.layer.borderColor = UIColor.lightGray.cgColor
            base.backgroundColor = UIColor.white
            base.layer.borderWidth = 1
            let leadingConstraint = NSLayoutConstraint(item: base, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 0)
            let trailingConstraint = NSLayoutConstraint(item: base, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0)
            let topConstraint = NSLayoutConstraint(item: base, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: 0)
            let bottomConstraint = NSLayoutConstraint(item: base, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
            base.addConstraints([leadingConstraint, trailingConstraint, topConstraint, bottomConstraint])
            let ico = self.stack.aspects[index]
            view.index = self.stack.index
            
            if AppManager.℠.currentMode == .oneCard {
                
            } else {
                
            }
        }
        return base
    }
    
    func koloda(_ koloda: KolodaView, viewForCardOverlayAt index: Int) -> OverlayView? {
        return OverlayView.init()
    }
    
    func koloda(_ koloda: KolodaView, didShowCardAt index: Int) {
        let view = self.kolodaView.viewForCard(at: index)!
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.slideCoin), object: index)
    }
    
    func koloda(_ koloda: KolodaView, shouldDragCardAt index: Int) -> Bool {
        return self.isMovable
    }
    
    func koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.updateCoin), object: stack.index)
    }
    
    func koloda(_ koloda: KolodaView, allowedDirectionsForIndex index: Int) -> [SwipeResultDirection] {
        return [SwipeResultDirection.left, .right]
    }
    
}
